const swiper = new Swiper(".swiper", {
	direction: "horizontal",
	slidesPerView: 1,
	spaceBetween: 30,
	loop: true,
	grabCursor: true,
	navigation: {
		nextEl: ".swiper-button-next",
		prevEl: ".swiper-button-prev",
	 },
	pagination: {
	  el: ".swiper-pagination",
	  clickable: true,
	},
	breakpoints: {
		375: {
			slidesPerView: 1,
			width: 255,
			loop: false,
		},
		425: {
			slidesPerView: 1,
			width: 255,
			loop: false,
		},
		768: {
			slidesPerView: 1,
			width: 255,
			loop: false,
		},
		1024: {
			slidesPerView: 3,
		},
		1260: {
			slidesPerView: 4,
		}
	}
 });